import React,{Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import ListPage from './pages/ListPage'

class App extends Component{
    render(){
        return(
            <Switch>
                <Route path='/posts' component={ListPage} />
            </Switch>
        )
    }
};
export default App;