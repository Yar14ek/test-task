import React,{Component} from 'react';
import {connect} from 'react-redux'
import {initPostList,deletePostElem} from '../../action/post-list-action'
import PostServices from '../../services/post-services'
import { Link } from "react-router-dom"


class PostList extends Component {
    postService = new PostServices()
    _apiUrl = 'https://bloggy-api.herokuapp.com/posts/';
    
    componentDidMount(){
            const {initPostList} = this.props

            this.postService.getMetodItems(this._apiUrl).then(posts=>{
            initPostList(posts)
        })
    }

    render(){
        const {postItems} = {...this.props};
        console.log(this.props)
        const listElem = postItems.map(el=>{
            return <Link key={el.id} >
                <h2>{el.title}</h2>
                <p>{el.body}</p>
                <button onClick={()=>this.postService.delMethodItem(this._apiUrl,el.id)/*.then(elem=>console.log('then',elem))*/}>del</button>
            </Link>
        })
        return(
            <>
            <ul>
                {listElem}
            </ul>
            </>
        )
    }
};

const mapStateToProps = state =>{
    return{
        postItems: state.appReduser.postList,
    }
}

const masDispatchToProps = dispatch => {
    return {
        initPostList: (posts) => 
            {dispatch(initPostList(posts)
            )},
        deleteElem:(url,id)=>{
            dispatch(deletePostElem(url,id)
            )},
    }
}

export default connect(mapStateToProps, masDispatchToProps)(PostList);