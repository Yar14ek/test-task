import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import rootReduser from './redusers/root-reducer'

const store = createStore(rootReduser,applyMiddleware(thunk,logger))
export default store;