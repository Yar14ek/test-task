import {combineReducers} from 'redux'
import appReduser from './post-list-reduser'

const rootReduser = combineReducers({
    appReduser
})
export default rootReduser;