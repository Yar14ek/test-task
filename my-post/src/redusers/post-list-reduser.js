import{
    INIT_POST_LIST,
    DEL_POST_ELEM
} from '../action/post-list-action'

const initialState = {
    postList: [],
    newPost:{
        body:'',
        title:'',
        id:'',
    }
}

const appReduser = (state = initialState, action)=>{
    switch(action.type) {
        case INIT_POST_LIST:{
            return {...state , postList:action.payload}
        }

        case DEL_POST_ELEM:{
            console.log(action)
            return{...state, postlist:action.payload}
        }

        default:
            return state;
    }
}
export default appReduser;